

class AlphabetChecker:

    def __init__(self, input_word, parent):
        self._parent = parent
        self._word = input_word

    @property
    def word(self):
        return self._word

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self,new_parent):
        if type(new_parent) != type("str"):
            return False
        else:
            self._parent = new_parent

    @word.setter
    def word(self, new_input_word):
        if type(new_input_word) != type("str"):
            return False
        else:
            self._word = new_input_word

    def cross_check(self):
        for letter in self._word:
            letter = letter.lower()
            if any(letter in elem for elem in self._parent):
                self._parent = self._parent.replace(letter,"")

        return self._parent, len(self._parent)


