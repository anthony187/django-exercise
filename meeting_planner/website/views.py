from datetime import datetime
from django.shortcuts import render
from meetings.models import Meeting
from django.http import HttpResponse
# Create your views here.


def welcome(request):
    return render(request,"website/welcome.html",
                  {"meetings": Meeting.objects.all()})


def date(request):
    return HttpResponse("This page was served at " + str(datetime.now()))


def about(request):
    return HttpResponse("I am just trying to learn Django. Hello.")


